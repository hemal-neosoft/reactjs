function Slider(){
    return (
        <div>
          <div id="carouselExampleSlidesOnly" class="carousel slide" data-bs-ride="carousel">
            <div class="carousel-inner">
              <div class="carousel-item active">
                <img src="./s1.jpg" class="d-block w-100" alt="Slide1" />
              </div>
              <div class="carousel-item">
                <img src="./s2.png" class="d-block w-100" alt="Slide 2" />
              </div>
              <div class="carousel-item">
                <img src="./s3.jpg" class="d-block w-100" alt="Slide 3" />
              </div>
            </div>
          </div>   
        </div>
    )
}

export default Slider;
