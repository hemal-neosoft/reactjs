function Cake(props){
if(props.data){
    return (
      <div className="col-sm-3">
      <div class="card" style={{width:"18rem"}}>
  <img class="card-img-top" src={props.data.image} alt="Card image cap" style={{width:"17.5rem",height:"17.5rem"}} />
  <div class="card-body">
    <h5 class="card-title">{props.data.name}</h5>
    <p class="card-text">{props.data.price}</p>
  </div>
</div>
</div>
    )
  } else {
    return null
  }
}

export default Cake;
