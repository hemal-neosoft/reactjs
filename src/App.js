import logo from './logo.svg';
import './App.css';
import Navbar from './components/Navbar';
import Slider from './components/Slider';
import Cakelist from './components/Cakelist';

var details = {
  username : "Hemal",
  technology : "PHP"
}

function App() {
  return (
    <div className="App">
      <Navbar details={details}></Navbar>
      <Slider></Slider>
      <Cakelist />
    </div>
  );
}

export default App;
